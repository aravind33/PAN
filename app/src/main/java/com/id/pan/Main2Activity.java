package com.id.pan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Main2Activity extends AppCompatActivity {
    private LinearLayout liner;
    private EditText ed7;
    private Button valid;
    private FirebaseDatabase mdb;
    private TextView txt,txt2,txt3;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mdb = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();

        ed7 = (EditText)findViewById(R.id.editText7) ;
        txt = (TextView)findViewById(R.id.Name);
        txt2 = (TextView)findViewById(R.id.Fname);
        txt3 = (TextView)findViewById(R.id.PAN);
        liner  = (LinearLayout)findViewById(R.id.Liner);
        liner.setVisibility(View.GONE);
        valid = (Button)findViewById(R.id.button2);
        valid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liner.setVisibility(View.VISIBLE);
                final String num = ed7.getText().toString();
                mdb.getReference();
                Query q = database.orderByChild(num);
                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
                            model mod = singleSnapshot.getValue(model.class);
                            if(mod.getPan().toString().contains(num)){
                                String name = mod.getNam();
                                String fName = mod.getFnames();
                                txt.setText("Name:"+name);
                                txt2.setText("Father's Name :"+fName);
                                txt3.setText("DOB:"+mod.getDate()+"/"+mod.getMon()+"/"+mod.getYear());
                            }
                            else {
                                Toast.makeText(Main2Activity.this, "No Record Found", Toast.LENGTH_SHORT).show();
                                txt.setText("");
                                txt2.setText("");
                                txt3.setText("");
                            }

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });
    }
}
