package com.id.pan;

/**
 * Created by Aravind on 29-08-2017.
 */

public class model {
    public String nam;
    public String fnames;
    public String pan;
    public String  mon,date,year;

    public void setNam(String nam) {
        this.nam = nam;
    }

    public void setFnames(String fnames) {
        this.fnames = fnames;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public void setDate(String  date) {
        this.date = date;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNam() {
        return nam;
    }

    public String getFnames() {
        return fnames;
    }

    public String getPan() {
        return pan;
    }

    public String getMon() {
        return mon;
    }

    public String getDate() {
        return date;
    }

    public String getYear() {
        return year;
    }
}
