package com.id.pan;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    private EditText Name,Fname,PAN,Mon,Date,Year;
    private Button Reg,show;
    private FirebaseDatabase mdb;
    private FirebaseAuth mAuth;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mdb = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        final model  mod = new model();

        Name = (EditText)findViewById(R.id.editText);
        Fname = (EditText)findViewById(R.id.editText2);
        PAN = (EditText)findViewById(R.id.editText4);
        Mon = (EditText)findViewById(R.id.editText6);
        Date = (EditText)findViewById(R.id.editText3);
        Year = (EditText)findViewById(R.id.editText5);
        Reg = (Button)findViewById(R.id.button);
        show = (Button)findViewById(R.id.button4);
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Nam = Name.getText().toString();
                String Pan = PAN.getText().toString();
                String FN = Fname.getText().toString();
                String mon = Mon.getText().toString();
                String Dat = Date.getText().toString();
                String year = Year.getText().toString();
                mod.setNam(Nam);
                mod.setFnames(FN);
                mod.setPan(Pan);
                mod.setDate(Dat);
                mod.setMon(mon);
                mod.setYear(year);
                mdb.getReference().child(Pan).setValue(mod);
                    Name.setText("");
                    Fname.setText("");
                    PAN.setText("");
                    Mon.setText("");
                    Date.setText("");
                    Year.setText("");
                    Toast.makeText(MainActivity.this, "Successfully Registered!", Toast.LENGTH_SHORT).show();


            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,Main2Activity.class);
                startActivity(i);

            }
        });
    }

}
